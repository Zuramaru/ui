﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sample4_UIManager : MonoBehaviour
{
    private Transform parentView;

    private void Awake()
    {
        parentView = GetComponentInChildren<RectTransform>().transform;
        ChangeView(UIView.Main, UIState.Open);
    }

    public void ChangeView(UIView view, UIState viewState)
    {
        Sample4_FactoryView.CreateView(view, parentView);
    }

}
