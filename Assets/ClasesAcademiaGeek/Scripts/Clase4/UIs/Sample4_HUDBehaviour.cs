﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sample4_HUDBehaviour : Sample4_UIBase
{
    private Transform parentView;
    public void OpenSettings()
    {
        parentView = GetComponentInChildren<RectTransform>().transform.parent;
        ChangeView(UIView.Settings, UIState.Open);
    }

    public void ChangeView(UIView view, UIState viewState)
    {
        Sample4_FactoryView.CreateView(view, parentView);
        CloseView();
    }
    public override void CloseView()
    {
        base.CloseView();
    }
}
