﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sample4_UIBase : MonoBehaviour
{
    public virtual void OpenView()
    {

    }

    public virtual void CloseView()
    {
        Destroy(this.gameObject);
    }
}
