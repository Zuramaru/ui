﻿using UnityEngine;

public class Sample4_FactoryView
{
    //Crea modulos segun el Nombre.
    public static Sample4_UIBase CreateView(UIView view, Transform parent)
    {
        string path = "Views/" + view.ToString();
        GameObject newView = GameObject.Instantiate(Resources.Load(path)) as GameObject;
        if (newView != null)
        {
            newView.transform.parent = parent.transform;
            newView.transform.localPosition = Vector3.zero;
            newView.transform.localScale = Vector3.one;
        }
        return newView.GetComponent<Sample4_UIBase>();
    }

    
}
